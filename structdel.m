% This code was obtained from a previous publication (Tang et
% al., Nat Commun, 2013) and authored by Erik Larsson Lekholm.

function struct = sort(struct, order, f_dim)
% delete elements in all variables of struct according to idx,
% but only those where one of the dimensions matches f_dim
if(~exist('f_dim'))
    f_dim = length(order);
end
cellfun(@do_del, fieldnames(struct))

    function [] = do_del(f_in)
        if size(struct.(f_in),1) == f_dim
            struct.(f_in)(order,:) = [];
        elseif size(struct.(f_in),2) == f_dim
            struct.(f_in)(:,order) = [];
    end
end
    
end