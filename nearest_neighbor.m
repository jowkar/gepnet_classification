%% Set up workspace
cd '~/proj/si_net_code'
outdir='~/proj/si-net/Manuscript/code/';
if exist(outdir,'dir')~=7
   mkdir(outdir)
end

load ~/proj/si-net/Manuscript/code/gencode;
clear gencode_exons; % save memory
load ~/proj/si-net/Manuscript/code/classes;

% load cashed expression data - initially read by load_expr.m
load ~/proj/si-net/Manuscript/code/expr;

for i = 1:length(expr.barcode)
    expr.sample_id{i} = expr.barcode{i}(1:16);
    expr.sample_type(i) = str2num(expr.sample_id{i}(14:15));
end

% Remove low-coverage and non-hiseq samples
expr = structdel(expr, strcmp(expr.platform, 'Illumina Genome Analyzer II'), length(expr.platform));
expr = structdel(expr, sum(expr.data) < 20e6, length(expr.platform));

% Only tumors/metastases
expr = structdel(expr, expr.sample_type >= 10, length(expr.sample_id)); 

% Load "in-house" data
%indir = '/Users/JoakimKarlsson/aibase/store3/local/proj/si-net/RNA-seq/preprocessing/aligned_hisat/';
indir = './gene_counts/';
s=dir(indir);
idx = cellfun(@(x) ~isempty(x),strfind({s.name},'.gene_counts'));
sname = {s.name};
sname = cell2mat(sname(idx)');
all_samples = unique(cellfun(@(x) regexprep(x,'.gene_counts',''),cellstr(sname),'UniformOutput',false));
expr_inhouse = read_rnaseq_htseq(all_samples, all_samples, indir, '.gene_counts');
expr_inhouse = structdel(expr_inhouse, ~ismember(expr_inhouse.gene_id, gencode.geneId), length(expr_inhouse.gene_id)); % remove a few genes not in annotation

% RPKM normalization - non-rubost version (as opposed to the TCGA data - but does not affect correlations)
depth = sum(expr_inhouse.data, 1);
expr_inhouse.data_norm = expr_inhouse.data./repmat(depth, size(expr_inhouse.data, 1), 1)*1e6; % M
expr_inhouse.data_norm = expr_inhouse.data_norm./repmat(expr.len, 1, size(expr_inhouse.data, 2))*1e3;  % RPK

%% Write data to file for t-SNE analysis in R (coding genes only)

% TCGA data
expr_norm_pancan_samples = cell2table(expr.sample_id');
writetable(expr_norm_pancan_samples,[outdir 'expr_norm_pancan_samples.txt'])

expr_norm_pancan_cancers = cell2table(expr.cancer_type');
writetable(expr_norm_pancan_cancers,[outdir 'expr_norm_pancan_cancers.txt'])

idx_coding = ismember(expr.gene_id, code);

expr_pancan = array2table(expr.data_norm);
writetable(expr_pancan(idx_coding,:),[outdir 'expr_norm_pancan_coding.txt'])

expr_norm_pancan_genes_symb = cell2table(expr.gene_symb');
writetable(expr_norm_pancan_genes_symb(idx_coding,:),[outdir 'expr_norm_pancan_genes_symb_coding.txt'])

expr_norm_pancan_genes_ens = cell2table(expr.gene_id);
writetable(expr_norm_pancan_genes_ens(idx_coding,:),[outdir 'expr_norm_pancan_genes_ens_coding.txt'])

% Non-TCGA samples
idx_coding = ismember(expr_inhouse.gene_id, code);
 
expr_inhouse_table = array2table(expr_inhouse.data_norm);
writetable(expr_inhouse_table(idx_coding,:),[outdir 'expr_norm_inhouse_coding.txt'])
 
isequal(expr_inhouse.gene_id,expr.gene_id)
expr_inhouse.gene_symb = expr.gene_symb;
expr_norm_inhouse_genes_symb = cell2table(expr_inhouse.gene_symb');
writetable(expr_norm_inhouse_genes_symb(idx_coding,:),[outdir 'expr_norm_inhouse_genes_symb_coding.txt'])
 
expr_norm_inhuse_genes_ens = cell2table(expr_inhouse.gene_id);
writetable(expr_norm_inhuse_genes_ens(idx_coding,:),[outdir 'expr_norm_inhouse_genes_ens_coding.txt'])
 
expr_norm_inhouse_samples = cell2table(expr_inhouse.sample_id');
writetable(expr_norm_inhouse_samples,[outdir 'expr_norm_inhouse_samples.txt'])

%% Calculate correlations to TCGA samples
clear r;
idx_coding = ismember(expr.gene_id, code);
for j = 1:size(expr_inhouse.data, 2)
	j
	for i = 1:length(expr.sample_id)
		r(i, j) = corr(expr_inhouse.data_norm(idx_coding, j), expr.data_norm(idx_coding, i), 'type', 'Spearman');
	end
end

%% List the top 10 most highly correlated samples

diary([outdir 'top10_similar.txt'])

for j = 1:size(r, 2)
	fprintf('%s--------------------------------:\n', expr_inhouse.sample_id{j});
	[~, idx] = sort(r(:, j), 'descend');
	for i = 1:10
		fprintf('%d\t%1.2f\t%s\t%s\n', i, r(idx(i), j), expr.cancer_type{idx(i)}, expr.sample_id{idx(i)});
	end
	fprintf('\n');
end
diary off

%% Predict using kNN
k = 6;
predicted = cell(size(expr_inhouse.data, 2),1);
for j = 1:size(expr_inhouse.data, 2)
   for kk = k:-1:1 %handle ties by iteratively reducing by 1
       [~, idx] = sort(r(:, j), 'descend');
       x = expr.cancer_type(idx(1:kk));
       y = unique(x);
       n = zeros(length(y), 1);
       for iy = 1:length(y)
         n(iy) = length(find(strcmp(y{iy}, x)));
       end
       [mx, itemp] = max(n);

       if length(find(mx==n))==1
           predicted(j) = y(itemp);
           break
       end
   end
end

tab=table(expr_inhouse.sample_id,predicted);
tab.Properties.VariableNames = {'Sample','Prediction'};

writetable(tab,[outdir 'kNN_prediction.txt'],'Delimiter','tab')
